function [alpham] = findStepBisection(f, x, d, maxStep, bisectionEps)
    section = [0; maxStep];
    L = section(2) - section(1);
    alpham = sum(section) / 2;
    fxm = f(x+alpham*d);
    
    while(L > 2 * bisectionEps)
        alpha = section + [L / 4; - L/4];
        fx1 = f(x+alpha(1)*d);
        fx2 = f(x+alpha(2)*d);
        if(fx1 < fxm)
            section(2) = alpham;
            alpham = alpha(1);
            fxm = fx1;
        elseif(fx2 < fxm)
            section(1) = alpham;
            alpham = alpha(2);
            fxm = fx2;
        else
            section = alpha;
        end
        L = section(2) - section(1);
    end
    alpham = sum(section) / 2;