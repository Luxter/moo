function [xArr, yArr, zArr] = appendPoint(f, x, xArr, yArr, zArr)
    xArr = [xArr; x(1)];
    yArr = [yArr; x(2)];
    zArr = [zArr, f(x)];
end