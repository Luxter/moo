function [x, xArr, yArr, zArr] = doTestStepForDirection(f, x, directions, index, step, xArr, yArr, zArr, plotTestStep)
    testX = x + step * directions(index,:)';
    if (plotTestStep == 1)
        [xArr, yArr, zArr] = appendPoint(f, x, xArr, yArr, zArr);
        [xArr, yArr, zArr] = appendPoint(f, testX, xArr, yArr, zArr);
    end;
    if(f(testX) < f(x))
        x = testX;
    else
        testX = x + -step * directions(index,:)';
        if (plotTestStep == 1)
            [xArr, yArr, zArr] = appendPoint(f, x, xArr, yArr, zArr);
            [xArr, yArr, zArr] = appendPoint(f, testX, xArr, yArr, zArr);
        end;
        if(f(testX) < f(x))
            x = testX;
        end;
    end;
    index = index + 1;
    if (index <= length(directions))
        [x, xArr, yArr, zArr] = doTestStepForDirection(f, x, directions, index, step, xArr, yArr, zArr, plotTestStep);
    end;
end