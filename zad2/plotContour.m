function plotContour(f, xArr, yArr, zArr)
    disp(min(xArr));
    [X,Y] = meshgrid(min(xArr)-0.5:(max(xArr)-min(xArr)+1)/100:max(xArr)+0.5, min(yArr)-0.5:(max(yArr)-min(yArr)+1)/100:max(yArr)+0.5);
    Z=arrayfun(@(x,y)f([x;y]), X, Y);
    contour(X,Y,Z,30);
    hold on;
    plot3(xArr, yArr, zArr,'-r*','LineWidth',2);
    hold off;
end