function [x, xArr, yArr, zArr] = findMinimumUsingQuasiNewton(startX, f, maxIterations, maxEpsilon, maxStep, bisectionEps)
    x=startX;
    H = eye(length(x));

    gradient = findGradient(f, x);
    i = 0;
    epsilon = 1;

    xArr(1) = x(1);
    yArr(1) = x(2);
    zArr(1) = f(x);
    fprintf('Start from: [%f; %f; %f]\n', x(1), x(2), f(x));    

    while(maxIterations > i || maxEpsilon < epsilon)
        direction = - H * gradient;
        step = findStepBisection(f, x, direction, maxStep, bisectionEps);
        xNew = x + step * direction;
        deltaX = xNew-x;

        gNew = findGradient(f, xNew);
    
        deltaGradient = gNew - gradient;

        HNew = H + (deltaX * deltaX')/(deltaGradient' * deltaX) - (((H * deltaGradient) * deltaGradient') * H)/((deltaGradient' * H) * deltaGradient);

        epsilon = abs(f(xNew)-f(x));
        gradient = gNew;
        x = xNew;
        H = HNew;
        i = i + 1;
        xArr(i+1) = x(1); %#ok<AGROW>
        yArr(i+1) = x(2); %#ok<AGROW>
        zArr(i+1) = f(x); %#ok<AGROW>
        fprintf('Iteration %i: [%f; %f; %f]\n', i, x(1), x(2), f(x));
    end
end