function [x, xArr, yArr, zArr] = findMinimumUsingHookeJeeves(startX, f, maxIterations, maxEpsilon, maxStep, stepCorrection, plotTestStep)
    directions = eye(length(startX));
    step = maxStep;
    x = startX;
    
    xArr = x(1);
    yArr = x(2);
    zArr = f(x);
    
    i = 1;
    baseX=x;
    while(maxIterations > i || step > maxEpsilon)
        [x, xArr, yArr, zArr] = doTestStepForDirection(f, x, directions, 1, step, xArr, yArr, zArr, plotTestStep); 
        fprintf('Test %i: [%f; %f; %f]\n', i, x(1), x(2), f(x));
        if(f(x)<f(baseX));
            disp(baseX);
            x = 2 * x - baseX;
            [xArr, yArr, zArr] = appendPoint(f, x, xArr, yArr, zArr);
            baseX = x;
        else
            x=baseX;
            [xArr, yArr, zArr] = appendPoint(f, x, xArr, yArr, zArr);
            step = stepCorrection * step;
        end;
        i = i + 1;
        fprintf('Iteration %i: [%f; %f; %f]\n', i, x(1), x(2), f(x));
    end
end