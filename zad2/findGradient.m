function [gradient] = findGradient(f, x)
    gradient = zeros(length(x),1);
    gradientParameter = 0.0001;
    gradientMatrix = eye(length(x)).*gradientParameter;
    for j=1:length(x)
        gradient(j) = (f(x + gradientMatrix(:,j)) - f(x))/gradientParameter;
    end
end