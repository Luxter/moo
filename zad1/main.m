function varargout = main(varargin)
% MAIN MATLAB code for main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN.M with the given input arguments.
%
%      MAIN('Property','Value',...) creates a new MAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before main_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to main_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help main

% Last Modified by GUIDE v2.5 20-Mar-2016 15:49:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @main_OpeningFcn, ...
                   'gui_OutputFcn',  @main_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before main is made visible.
function main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to main (see VARARGIN)

% Choose default command line output for main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes main wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = main_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in stopMethod.
function stopMethod_Callback(hObject, eventdata, handles)
% hObject    handle to stopMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns stopMethod contents as cell array
%        contents{get(hObject,'Value')} returns selected item from stopMethod


% --- Executes during object creation, after setting all properties.
function stopMethod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to stopMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function stopValue_Callback(hObject, eventdata, handles)
% hObject    handle to stopValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of stopValue as text
%        str2double(get(hObject,'String')) returns contents of stopValue as a double


% --- Executes during object creation, after setting all properties.
function stopValue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to stopValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in start.
function start_Callback(hObject, eventdata, handles)
% hObject    handle to start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


leftEdge = str2double(get(handles.leftEdge, 'String'));
rightEdge = str2double(get(handles.rightEdge, 'String'));
if(leftEdge >= rightEdge)
   msgbox('Nieprawid�owe kra�ce przedzia�u', 'B��d', 'error');
end
sectionCount = str2double(get(handles.sectionCount, 'String'));

stringFunction = get(handles.inputFunction, 'String');
inputFunction = str2func(strcat('@(x)', stringFunction));

methods = get(handles.method, 'String');
currentMethod = methods{get(handles.method, 'Value')};

stopMethodIndex = get(handles.stopMethod, 'Value');
stopMethods = get(handles.stopMethod, 'String');
stopMethod = stopMethods{stopMethodIndex};
stopValue = str2double(get(handles.stopValue, 'String'));

if(checkUnimodalSection(leftEdge, rightEdge, inputFunction, sectionCount))
    unimodalSection = [leftEdge; rightEdge];
else
    unimodalSection = findUnimodalSection(leftEdge, rightEdge, inputFunction, sectionCount);
    if(length(unimodalSection) == 0)
        msgbox('Nie istnieje przedzia� unimodalny', 'B��d', 'error');
        return
    end
end

switch(currentMethod)
    case 'Metoda bisekcji'
        if(strcmp(stopMethod, 'Ilo�� iteracji'))
            [x1min, x2min, min] = bisectionIt(unimodalSection, inputFunction, stopValue);
        else
            [x1min, x2min, min] = bisectionEps(unimodalSection, inputFunction, stopValue);
        end
        matlabMin = fminbnd(inputFunction, unimodalSection(1), unimodalSection(2));
    case 'Metoda dychotomii'
        dichotomyParam = str2double(get(handles.d, 'String'));
        
        if(strcmp(stopMethod, 'Ilo�� iteracji'))
            [x1min, x2min, min] = dichotomyIt(unimodalSection, inputFunction, stopValue, dichotomyParam);
        else
            if(dichotomyParam > stopValue)
                msgbox('Podana odleg�o�� musi by� mniejsza od wybranej dok�adno�ci', 'B��d', 'error');
                return
            end
            [x1min, x2min, min] = dichotomyEps(unimodalSection, inputFunction, stopValue, dichotomyParam);
        end
        matlabMin = fminbnd(inputFunction, unimodalSection(1), unimodalSection(2));
end

figure;
h1 = plot(linspace(leftEdge, rightEdge, 200), arrayfun(inputFunction, linspace(leftEdge, rightEdge, 200)), 'k');
hold on;
h2 = plot(linspace(unimodalSection(1), unimodalSection(2), 200), arrayfun(inputFunction, linspace(unimodalSection(1), unimodalSection(2), 200)), 'g', 'LineWidth', 2);
hold on;
h3 = plot(linspace(x1min, x2min, 200), arrayfun(inputFunction, linspace(x1min, x2min, 200)), 'r', 'LineWidth', 3);
hold on;
h4 = plot(min, inputFunction(min), '*');
hold off;
legend([h1, h2, h3, h4], stringFunction, 'przedzia� unimodalny', 'przedzia� ko�cowy', 'minimum', 'Location','southoutside')
 
msgbox(sprintf('Znaleziony przedzia�: (%.8f, %.8f).\nObliczone minimum = %f, wzorcowe minimum = %f.', x1min, x2min, min, matlabMin), 'Obliczenia zako�czone');

function leftEdge_Callback(hObject, eventdata, handles)
% hObject    handle to leftEdge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of leftEdge as text
%        str2double(get(hObject,'String')) returns contents of leftEdge as a double


% --- Executes during object creation, after setting all properties.
function leftEdge_CreateFcn(hObject, eventdata, handles)
% hObject    handle to leftEdge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rightEdge_Callback(hObject, eventdata, handles)
% hObject    handle to rightEdge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rightEdge as text
%        str2double(get(hObject,'String')) returns contents of rightEdge as a double


% --- Executes during object creation, after setting all properties.
function rightEdge_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rightEdge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in method.
function method_Callback(hObject, eventdata, handles)
    methods = get(handles.method, 'String');
    currentMethod = methods{get(handles.method, 'Value')};
    switch(currentMethod)
        case 'Metoda bisekcji'
            set(handles.text7,'visible','off');
            set(handles.d,'visible','off');
        case 'Metoda dychotomii'
            set(handles.text7,'visible','on');
            set(handles.d,'visible','on');
    end


% hObject    handle to method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns method contents as cell array
%        contents{get(hObject,'Value')} returns selected item from method


% --- Executes during object creation, after setting all properties.
function method_CreateFcn(hObject, eventdata, handles)
% hObject    handle to method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function inputFunction_Callback(hObject, eventdata, handles)
% hObject    handle to inputFunction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inputFunction as text
%        str2double(get(hObject,'String')) returns contents of inputFunction as a double


% --- Executes during object creation, after setting all properties.
function inputFunction_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inputFunction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sectionCount_Callback(hObject, eventdata, handles)
% hObject    handle to sectionCount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sectionCount as text
%        str2double(get(hObject,'String')) returns contents of sectionCount as a double


% --- Executes during object creation, after setting all properties.
function sectionCount_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sectionCount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function d_Callback(hObject, eventdata, handles)
% hObject    handle to d (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of d as text
%        str2double(get(hObject,'String')) returns contents of d as a double


% --- Executes during object creation, after setting all properties.
function d_CreateFcn(hObject, eventdata, handles)
% hObject    handle to d (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
