function unimodalSection = findUnimodalSection(a, b, f, n)
    deltax = (b - a) / n;
    x = a : deltax : b;
    y = arrayfun(f, x);
    yDiff = diff(y); %r�nice mi�dzy kolejnymi elementami
    yDiffNonPos = yDiff <= 0; %warto�ci niedodatnie r�nic (funkcja nie ro�nie)
    yDiffNonNeg = yDiff >= 0; %warto��i nieujemne r�nic (funkcja nie maleje)
    yDiffNonPosShifted = yDiffNonPos(1:end-1); %ucinamy ostatni� warto��
    yDiffNonNegShifted = yDiffNonNeg(2:end); %przesuwamy o 1 w lewo (ucinamy pierwsz� warto��)
    %{
    Je�li w obu wektorach na tym samym indeksie jest warto�� 1 to znaczy, 
    �e niedodatnia r�nica poprzedza nieujemn�, czyli jest to przedzia� unimodalno�ci
    %}
    yDiffNonPosBeforeNonNeg = yDiffNonPosShifted .* yDiffNonNegShifted;
    unimodalSectionBeginning = x(find(yDiffNonPosBeforeNonNeg, 1)); %pocz�tek przedzia�u
    unimodalSectionEnding = x(find(yDiffNonPosBeforeNonNeg, 1)+2); %koniec przedzia�u
    unimodalSection = [unimodalSectionBeginning; unimodalSectionEnding]; %przedzia�
end

