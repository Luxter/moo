function isUnimodal = checkUnimodalSection(a, b, f, n)
    deltax = (b - a) / n;
    x = a : deltax : b;
    y = arrayfun(f, x);
    yDiff = diff(y); %r�nice mi�dzy kolejnymi elementami
    yDiffNonPos = yDiff <= 0; %warto�ci niedodatnie r�nic (funkcja nie ro�nie)
    yDiffNonNeg = yDiff >= 0; %warto��i nieujemne r�nic (funkcja nie maleje)
    yDiffNonPosShifted = yDiffNonPos(1:end-1); %ucinamy ostatni� warto��
    yDiffNonNegShifted = yDiffNonNeg(2:end); %przesuwamy o 1 w lewo (ucinamy pierwsz� warto��)
    %{
    Je�li w obu wektorach na tym samym indeksie jest warto�� 1 to znaczy, 
    �e niedodatnia r�nica poprzedza nieujemn�, czyli jest to przedzia� unimodalno�ci
    %}
    yDiffNonPosBeforeNonNeg = yDiffNonPosShifted .* yDiffNonNegShifted;
    %{
    Zliczamy wszystkie minima lokalne na przedziale, je�li jest ich wi�cej
    ni� 1 to przedzia� nie jest unimodalny
    %}
    localMinCount = length(yDiffNonPosBeforeNonNeg(yDiffNonPosBeforeNonNeg==1));
    if(localMinCount == 1)
        isUnimodal = true;
    else
        isUnimodal = false;
    end
end

