function [x1min, x2min, min] = dichotomyIt(section, f, it, dichotomyParam)
    i = 0;
    while(i < it)
        currentSectionText = sprintf('Iteration: %d, epsilon: %d, section: [%f; %f]', i, (section(2) - section(1)), section);
        disp(currentSectionText);
        middle = (section(2) + section(1)) / 2;
        x1 = middle - dichotomyParam / 2;
        x2 = middle + dichotomyParam / 2;
        fx1 = f(x1);
        fx2 = f(x2);
        if(fx1 > fx2)
            section(1) = x1;
        elseif(fx1 < fx2)
            section(2) = x2;
        else
            section(1) = x1;
            section(2) = x2;
        end
        i = i + 1;
    end

    x1min = section(1);
    x2min = section(2);
    min = (section(1) + section(2)) / 2;
end

