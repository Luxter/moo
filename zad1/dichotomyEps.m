function [x1min, x2min, min] = dichotomyEps(section, f, eps, dichotomyParam)
    i = 0;
    while((section(2) - section(1)) > 2 * eps)
        currentSectionText = sprintf('Iteration: %d, section: [%f; %f]', i, section);
        disp(currentSectionText)
        middle = (section(2) + section(1)) / 2;
        x1 = middle - dichotomyParam / 2;
        x2 = middle + dichotomyParam / 2;
        fx1 = f(x1);
        fx2 = f(x2);
        if(fx1 > fx2)
            section(1) = x1;
        elseif(fx1 < fx2)
            section(2) = x2;
        else
            section(1) = x1;
            section(2) = x2;
        end
        i = i + 1;
    end

    x1min = section(1);
    x2min = section(2);
    min = (section(1) + section(2)) / 2;

end

%%str = 'x(2)^4 - 2*x(1)^2 + 5*x(1)^2*x(2)^3 - 8';
%%str = '1*(x(2)-(5.1/(4*pi^2))*x(1)^2+(5/pi)*x(1)-6)^2+10*(1-(1/(8*pi)))*cos(x(1))+10';
x(1)^2+x(2)^2
(x(1)+10*x(2))^2+5*(x(3)-x(4))^2+(x(2)-2*x(3))^4+10*(x(1)-x(4))^4
%str = '(1+(x(1)+x(2)+1)^2*(19-14*x(1)+3*x(1)^2-14*x(2)+6*x(1)*x(2)+3*x(2)^2))*(30+(2*x(1)-3*x(2))^2*(18-32*x(1)+12*x(1)^2+48*x(2)-36*x(1)*x(2)+27*x(2)^2))';
100*(x(2)-x(1)^2)^2+(1-x(1))^2 start z 3;3;