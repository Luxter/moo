function [x1min, x2min, min] = bisectionIt(section, f, it)
    L = section(2) - section(1);
    xm = (section(1) + section(2)) / 2;
    fxm = f(xm);
    
    i = 0;
    while(i < it)
        currentSectionText = sprintf('Iteration: %d, epsilon: %d, section: [%f; %f]', i, (section(2) - section(1)), section);
        disp(currentSectionText)
        x1 = section(1) + L / 4;
        x2 = section(2) - L / 4;
        fx1 = f(x1);
        fx2 = f(x2);
        if(fx1 < fxm)
            section(2) = xm;
            xm = x1;
            fxm = fx1;
        elseif(fx2 < fxm)
            section(1) = xm;
            xm = x2;
            fxm = fx2;
        else
            section(1) = x1;
            section(2) = x2;
        end
        L = section(2) - section(1);
        i = i + 1;
    end

    x1min = section(1);
    x2min = section(2);
    min = (section(1) + section(2)) / 2;
end
