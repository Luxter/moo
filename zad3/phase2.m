function [simplex, basicVars, unbounded] = phase2(simplex, basicVars, ZRow, artVarStartIdx, artVarEndIdx)
%PHASE2 Summary of this function goes here
%   Detailed explanation goes here
    disp('Second Phase')
    
    unbounded = false;
    
%     change the Z' row back to Z row
    simplex(end, 1:length(ZRow)) = ZRow;
    simplex(end, length(ZRow)+1:end) = 0;
    
%     remove possible artificial variables' columns
    j = artVarStartIdx;
    for i = artVarStartIdx : artVarEndIdx
        if(isempty(find(basicVars == j, 1)))
            simplex(:, j) = [];
            basicVars(basicVars > j) = basicVars(basicVars > j) - 1;
            j = j - 1;
        end
        j = j + 1;
    end
    
    disp('Simplex with original Z row and removed possible artificial columns')
    disp(simplex)
    disp(basicVars)
    
%     pivot basic variables
    for i=1:length(basicVars)
        rowToPivot = i;
        columnToPivot = basicVars(i);
        [simplex, basicVars]= pivot(simplex, basicVars, rowToPivot, columnToPivot);
        disp(simplex)
    end
    
%     until there are no negative coefficients in Z row
    while(sum(simplex(end, 1:end-1) < 0) > 0)
        [~, columnToPivot] = min(simplex(end, 1:end-1));
%         Removing non-positive ratios
        ratios = simplex(1:end-1, end)./simplex(1:end-1, columnToPivot);
        ratios(ratios <= 0) = Inf;
        [min2, rowToPivot] = min(ratios);
        if(min2 == Inf)
            unbounded = true;
            disp('brak (optimum w nieskończoności)') 
            break;
        end
%         Remove artificial variable column when it leaves base variables
        if(basicVars(rowToPivot) > artVarStartIdx)
            disp('Usuwanie artificial variable' + rowToPivot)
            simplex(:, rowToPivot) = [];
            disp(simplex)
        end

        [simplex, basicVars] = pivot(simplex, basicVars, rowToPivot, columnToPivot);
        disp(simplex)
    end
end

