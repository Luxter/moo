function [simplex, basicVars] = phase1(simplex, basicVars, artVarCount, artVarStartIdx, artVarEndIdx)
%PHASE1 Summary of this function goes here
%   Detailed explanation goes here
    disp('First Phase')
%     change the Z row for artificial variables minimalization Z'
    simplex(end, :) = 0;
    disp('Simplex with new Z row')
    simplex(end, artVarStartIdx : artVarEndIdx) = -1;
    disp(simplex)

%     only artificial variables are basic variables
    for i = 1:artVarCount
        columnToPivot = artVarStartIdx + i - 1;
        [~, rowToPivot] = min(simplex(1:end-1, end)./simplex(1:end-1, columnToPivot));
        
        [simplex, basicVars] = pivot(simplex, basicVars, rowToPivot, columnToPivot);
        disp(simplex)
    end

%     until there are no positive coefficients in Z' row
    while(sum(simplex(end, 1:end-1) > 0) > 0)
        [~, columnToPivot] = max(simplex(end, 1:end-1));
%         Removing non-positive ratios
        ratios = simplex(1:end-1, end)./simplex(1:end-1, columnToPivot);
        ratios(ratios <= 0) = Inf;
        [~, rowToPivot] = min(ratios);

        [simplex, basicVars] = pivot(simplex, basicVars, rowToPivot, columnToPivot);
        disp(simplex)
    end
end

