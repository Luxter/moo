function [] = displayResults(simplex, basicVars, func)
%DISPLAYRESULTS Summary of this function goes here
%   Detailed explanation goes here

    xCount = nnz(func);
% if nonbasic variable has coefficient 0 in Z row there are multiple solutions
    nonBasicVars = setdiff(1:size(simplex, 2)-1, basicVars);
    zeroNonBasicVar = nonBasicVars(find(isZero(simplex(end, nonBasicVars)), 1));

    if(~isempty(zeroNonBasicVar))          
        firstResults = zeros(xCount, 1);
        firstResults(basicVars(basicVars <= xCount)) = simplex(basicVars <= xCount, end);

        [simplex, basicVars] = pivot(simplex, basicVars, find(basicVars > xCount, 1), zeroNonBasicVar);
        disp(simplex);

        disp('Wiele rozwi�za� na odcinku');  

        secondResults = zeros(xCount, 1);
        secondResults(basicVars(basicVars <= xCount)) = simplex(basicVars <= xCount, end);

        results = [firstResults, secondResults];
        disp(results);

        optValue = func * firstResults;
        disp(optValue);
    else
        results = zeros(xCount, 1);
        results(basicVars(basicVars <= xCount)) = simplex(basicVars <= xCount, end);
        disp(results);

        optValue = func * results;
        disp(optValue);  
    end
end

