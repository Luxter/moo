% last column in constraint inequality sign (-1 for <, 0 for = and 1 for >)
% one before last column is RHS value in constraint inequality
inputFolder = 'input/';
simplex = dlmread(strcat(inputFolder, 'ftims9.txt'));

maximization = simplex(1, find(simplex(1, :) ~= 0, 1, 'last'));
% remove the maximization flag from simplex
simplex(1, find(simplex(1, :) ~= 0, 1, 'last')) = 0;
if(maximization == -1)
    maximization = false;
else
    maximization = true;
end

% remember function to calculate optimum value
func = simplex(1, simplex(1, 1:end-1) ~= 0);

% move Z row to the last row
simplex = circshift(simplex, [-1 0]);

[simplex, basicVars, artVarCount, artVarStartIdx, artVarEndIdx, f, A, b, Aeq, beq, lb] = convertToStandard(simplex, maximization);

% remember Z row for phase II
ZRow = simplex(end, 1:end-1);

[simplex, basicVars] = phase1(simplex, basicVars, artVarCount, artVarStartIdx, artVarEndIdx);

noSolutions = checkArtVars(simplex, basicVars, artVarStartIdx);

if(~noSolutions)
    [simplex, basicVars, unbounded] = phase2(simplex, basicVars, ZRow, artVarStartIdx, artVarEndIdx);
    
    if(~unbounded)
        displayResults(simplex, basicVars, func);
    end
end

displayLinprogResults(maximization, f, A, b, Aeq, beq, lb);

