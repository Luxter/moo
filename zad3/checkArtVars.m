function [noSolutions] = checkArtVars(simplex, basicVars, artVarStartIdx)
%CHECKARTVARS Summary of this function goes here
%   Detailed explanation goes here
%     Check if there are artificial variables in base variables
    noSolutions = false;
    for i = 1:length(basicVars)
        if(basicVars(i) >= artVarStartIdx && ~isZero(simplex(basicVars == basicVars(i), end)))
            disp('brak - sprzeczne ograniczenia, obszar dopuszczalny jest pusty')
            noSolutions = true;
            break;
        end
    end
end

