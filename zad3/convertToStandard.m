function [simplex, basicVars, artVarCount, artVarStartIdx, artVarEndIdx, f, A, b, Aeq, beq, lb] = convertToStandard(simplex, maximization)
%CONVERTTOSTANDARD Summary of this function goes here
%   Detailed explanation goes here

%     multiply rows by -1 when RHS < 0
    simplex(simplex(1:size(simplex, 1)-1, end-1) < 0, :) = -simplex(simplex(1:size(simplex, 1)-1, end-1) < 0, :);
    signs = simplex(1:end-1, end);
    
%     cut off column with signs
    simplex = simplex(:, 1:end-1);

    simplexRows = size(simplex, 1);
    simplexColumns = size(simplex, 2);
    
%     cut off Z equation
    constraintRows = simplexRows - 1;   

%     our starting basic variables
    basicVars = zeros(1, constraintRows);

    slackVarCount = length(signs(signs ~= 0));
    slackVarStartIdx = simplexColumns;
    slackVarEndIdx = slackVarStartIdx + slackVarCount - 1;
    artVarCount = length(signs(signs >= 0));
    artVarStartIdx = slackVarEndIdx + 1;
    artVarEndIdx = artVarStartIdx + artVarCount - 1;
%     submatrix for slack and artificial variables (last row empty for Z row)
    slackArtVars = zeros(simplexRows, slackVarCount + artVarCount);
    actualArtVarCount = 0;

%     f, A, b, Aeq, beq for matlab linprog
    inequalitiesCounter = 1;
    equalitiesCounter = 1;
    f = simplex(end, 1:slackVarStartIdx-1);
    A = zeros(constraintRows, slackVarStartIdx-1);
    b = zeros(constraintRows, 1);
    Aeq = zeros(length(signs(signs == 0)), slackVarStartIdx-1);
    beq = zeros(length(signs(signs == 0)), 1);
    lb = zeros(length(f), 1);

    for i = 1:constraintRows
%         <= inequaity constraint
        if(signs(i, 1) == -1)
            slackArtVars(i, i) = 1;
            basicVars(i) = slackVarStartIdx + i - 1;
            A(inequalitiesCounter, 1:slackVarStartIdx-1) = simplex(i, 1:slackVarStartIdx-1);
            b(inequalitiesCounter) = simplex(i, end);

            inequalitiesCounter = inequalitiesCounter + 1;
%             >= inequality constraint
        elseif(signs(i, 1) == 1)
            slackArtVars(i, i) = -1;
            slackArtVars(i, slackVarCount + actualArtVarCount + 1) = 1;
            basicVars(i) = artVarStartIdx + actualArtVarCount;
            actualArtVarCount = actualArtVarCount + 1;
            A(inequalitiesCounter, 1:slackVarStartIdx-1) = -simplex(i, 1:slackVarStartIdx-1);
            b(inequalitiesCounter) = -simplex(i, end);
            inequalitiesCounter = inequalitiesCounter + 1;
%             = equality constraint
        else
            slackArtVars(i, slackVarCount + actualArtVarCount + 1) = 1;
            basicVars(i) = artVarStartIdx + actualArtVarCount;
            actualArtVarCount = actualArtVarCount + 1;
            Aeq(equalitiesCounter, 1:slackVarStartIdx-1) = simplex(i, 1:slackVarStartIdx-1);
            beq(equalitiesCounter) = simplex(i, end);
            equalitiesCounter = equalitiesCounter + 1;
        end
    end

%     convert Z row into standard form
    if(maximization)
        simplex(end, :) = -simplex(end, :);
    end
    
    disp('Converted simplex');
    disp(simplex);
    
    disp('Simplex with slack and atrificial variables:')
    simplex = [simplex(:,1:end-1) slackArtVars simplex(:,end)];
    disp(simplex)
end

