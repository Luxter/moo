function [] = displayLinprogResults(maximization, f, A, b, Aeq, beq, lb)
%DISPLAYLINPROGRESULTS Summary of this function goes here
%   Detailed explanation goes here

%     maximization = -minimanization(-f)
    if(maximization)
        f = -f;
    end

    [x, fVal] = linprog (f, A, b, Aeq, beq, lb);
    disp(x);
    
    if(maximization)
        fVal = -fVal;
    end
    disp(fVal);
end

