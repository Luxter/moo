function [simplex, basicVars] = pivot(simplex, basicVars, row, column)
%PIVOT Summary of this function goes here
%   Detailed explanation goes here
    basicVars(row) = column;
    simplexRows = size(simplex, 1);
    if(simplex(row, column) ~= 1)
        simplex(row, :) = simplex(row, :) ./ simplex(row, column);
    end
    for i = 1:simplexRows
       if(i ~= row && simplex(i, column) ~= 0)
           multiplicationCoefficient = -(simplex(i, column)/simplex(row, column));
           simplex(i, :) = simplex(i, :) + simplex(row, :).*multiplicationCoefficient;
       end
    end
end

