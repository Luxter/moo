function [x, xArr, yArr, zArr, i, barrierLambda] = findMinimumUsingQuasiNewton(startX, f, maxIterations, maxEpsilon, maxStep, bisectionEps, barrierFunction, barrierLambdaCorrection)
    x=startX;
    H = eye(length(x));
    barrierLambda = 1;
    gradient = findGradient(f, x, barrierFunction, barrierLambda);
    i = 0;
    epsilon = 1;

    xArr(1) = x(1);
    yArr(1) = x(2);
    zArr(1) = f(x);
    %fprintf('[%.2f; %.2f]\n', x(1), x(2));    
    while(maxIterations > i || maxEpsilon < epsilon)
        
        direction = - H * gradient;
        step = findStepBisection(f, x, direction, maxStep, bisectionEps, barrierFunction, barrierLambda);
        xNew = x + step * direction;
        deltaX = xNew-x;

        gNew = findGradient(f, xNew, barrierFunction, barrierLambda);
    
        deltaGradient = gNew - gradient;

        HNew = H + (deltaX * deltaX')/(deltaGradient' * deltaX) - (((H * deltaGradient) * deltaGradient') * H)/((deltaGradient' * H) * deltaGradient);

        epsilon = norm(xNew-x);
        gradient = gNew;
        x = xNew;
        H = HNew;
        i = i + 1;
        
            barrierLambda = barrierLambdaCorrection * barrierLambda;
        
        xArr(i+1) = x(1); %#ok<AGROW>
        yArr(i+1) = x(2); %#ok<AGROW>
        zArr(i+1) = f(x); %#ok<AGROW>        
    end
    fprintf('%i & [%.2f; %.2f; %.2f]\n', i, x(1), x(2), f(x));
end