function [gradient] = findGradient(f, x, barrierFunction, barrierLambda)
    gradient = zeros(length(x),1);
    gradientParameter = 0.0001;
    gradientMatrix = eye(length(x)).*gradientParameter;
    for j=1:length(x)
        barrierSum = barrierValue(barrierFunction,x + gradientMatrix(:,j));
        barrierSumOld = barrierValue(barrierFunction,x);
        gradient(j) = ((f(x + gradientMatrix(:,j)) + barrierLambda * barrierSum) - (f(x) + barrierLambda * barrierSumOld))/gradientParameter;
    end
end