function varargout = main(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @main_OpeningFcn, ...
                   'gui_OutputFcn',  @main_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function main_OpeningFcn(hObject, ~, handles, varargin)
    handles.output = hObject;
    guidata(hObject, handles);

function varargout = main_OutputFcn(~, ~, handles) 
    varargout{1} = handles.output;

function updateButton_Callback(~, ~, handles) %#ok<DEFNU>

    maxStep = str2double(get(handles.step, 'String'));
    if (strcmp(get(handles.iterations,'enable'),'on'))
        maxIterations = str2double(get(handles.iterations, 'String'));
        meadOptions = optimset('MaxIter',maxIterations);
        quasiOptions = optimoptions(@fminunc,'MaxIter',maxIterations,'Algorithm','quasi-newton');
    else
        maxIterations = -1;
    end
    
    if (strcmp(get(handles.epsilon,'enable'),'on'))
        maxEpsilon = str2double(get(handles.epsilon, 'String'));
        meadOptions = optimset('TolX',maxEpsilon);
        quasiOptions = optimoptions(@fminunc,'TolX',maxEpsilon,'Algorithm','quasi-newton');
    else
        maxEpsilon = flintmax;
    end

    startX = str2num(get(handles.startPointInput, 'String')); %#ok<ST2NM>
    functionString = get(handles.functionInput, 'String');
    f = str2func(['@(x)' functionString]);
    barrierFunctionString1 = get(handles.barrierFunctionInput1, 'String');
    barrierFunctionString2 = get(handles.barrierFunctionInput2, 'String');
    barrierCounter = 1;
    if (~isempty(barrierFunctionString1))
        barrierFunction{barrierCounter} = str2func(['@(x)1/-(' barrierFunctionString1 ')']);
        barrierCounter = barrierCounter + 1;
    end
    if (~isempty(barrierFunctionString2))
        barrierFunction{barrierCounter} = str2func(['@(x)1/-(' barrierFunctionString2 ')']);
    end
    barrierCorrection = str2double(get(handles.barrierCorrectionInput, 'String'));
    %[meadX,mfval,~,moutput] = fminsearch(f,startX, meadOptions);

    %[quasiX,qfval,~,qoutput] = fminunc(f,startX, quasiOptions);

    bisectionEps = str2double(get(handles.bisectionEpsField, 'String'));
    
    [x, xArr, yArr, zArr, ~, barrierParam] = findMinimumUsingQuasiNewton(startX,f,maxIterations,maxEpsilon,maxStep,bisectionEps,barrierFunction,barrierCorrection);

    if (length(x) < 3)
        plotContour(f, xArr, yArr, zArr, barrierFunction, barrierParam);
    end;
    lb = [-100,-100];
ub = [3,2];
A = [];
b = [];
Aeq = [];
beq = [];
x0 = startX;
A = [1,2];
b = 1;
[wynik,~,~,output] = fmincon(f,x0,A,b)
%[wynik,~,~,output] = fmincon(f,x0,A,b,Aeq,beq,lb,ub)

function rotationModeButton_Callback(~, ~, ~) %#ok<DEFNU>
    rotate3d on;


function positionModeButton_Callback(~, ~, ~) %#ok<DEFNU>
    datacursormode on;

function stopCondition_Callback(hObject, ~, handles) %#ok<DEFNU>
    contents = cellstr(get(hObject,'String'));
    currentMethod = contents{get(hObject,'Value')};
    switch(currentMethod)
        case 'Liczba krok�w'
            set(handles.epsilon,'enable','off');
            set(handles.iterations,'enable','on');
        case 'Dok�adno��'
            set(handles.epsilon,'enable','on');
            set(handles.iterations,'enable','off');
    end

% --- Executes on selection change in methodComboBox.
function methodComboBox_Callback(~, ~, handles)%#ok<DEFNU>
contents = cellstr(get(handles.methodComboBox,'String'));
method = contents{get(handles.methodComboBox,'Value')};
switch(method)
	case 'Hooke-Jeeves'
    	set(handles.stepCorrectionField,'visible','on');
        set(handles.stepCorrectionLbl,'visible','on');
        set(handles.testStepButton,'visible','on');
        set(handles.bisectionEpsLbl,'visible','off');
    	set(handles.bisectionEpsField,'visible','off');
	case 'quasi-Newton'
        set(handles.bisectionEpsField,'visible','on');
        set(handles.stepCorrectionLbl,'visible','off');
        set(handles.testStepButton,'visible','off');
        set(handles.bisectionEpsLbl,'visible','on');
    	set(handles.stepCorrectionField,'visible','off');
end
