function [max] = findBarrierFunctionMaximum(barrierFunction, x, d, maxStep, bisectionEps)
    max = realmax;
    iterator = 1;
    while(iterator <= length(barrierFunction))
        section = [0; maxStep];
        L = section(2) - section(1);
        alpham = sum(section) / 2;
        fxm = norm(x+alpham*d);
  
        while(L > 2 * bisectionEps)
            alpha = section + [L / 4; -L/4];
            fx1 = barrierFunction{iterator}(x+alpha(1)*d);   
            fx2 = barrierFunction{iterator}(x+alpha(2)*d);
            if(fx2 > fxm && fx2 > 0)
                section(1) = alpham;
                alpham = alpha(2);
                fxm = fx2;
            elseif(fx1 > 0)
                section = alpha;
            else
                section(2) = alpham;
                alpham = alpha(1);
                fxm = fx1;
            end
            L = section(2) - section(1);
        end       
        iterator = iterator+1;
        if (alpham < max)
            max = alpham;           
        end
    end