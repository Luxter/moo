function [value] = barrierValue(barrierFunction,x)
    value = barrierFunction{1}(x);
    if(length(barrierFunction)>1)
        value = value + barrierFunction{2}(x);
    end